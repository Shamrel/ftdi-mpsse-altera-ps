/**
 \file
 \author Shauerman Alexander <shamrel@yandex.ru>  www.labfor.ru
 \details
 This utility can be used to configure Alters's FPGA via USB.
 It utilises the functionality of the proprietary Multi-Protocol Synchronous
 Serial Engine (MPSSE) architecture to adapt to the Altera Passive Serial (PS) interface.
 \version   0.1
 \date 5.10.2018
 \copyright
  BSD 2-Clause License
  Copyright (c) 2018, Shauerman Alexander.
  All rights reserved.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:

  1. Redistributions of source code must retain the above copyright notice, this
     list of conditions and the following disclaimer.

  2. Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#ifdef _WIN32
#include <windows.h>
#endif

#include "ftdi/ftd2xx.h"
#include "ftdi/ftdi_mpsse.h"

#define PRINT_DEVICE_LIST 0

#define PORT_DIRECTION  (0x07)
#define DCLK            (0)
#define DATA0           (1)
#define N_CONFIG        (2)
#define N_STATUS        (3)
#define CONF_DONE       (4)

// initial states of the MPSSE interface
#define DCLK_DEF            1
#define DATA0_DEF           0
#define N_CONFIG_DEF        1
#define N_STATUS_DEF        1
#define CONF_DONE_DEF       1

#define TIMEOUT_CONF_DONE   10
#define   DATA_SPEED  3000000ul

#define MPSSE_PCK_SEND_SIZE    512
BYTE buff[MPSSE_PCK_SEND_SIZE];

FT_HANDLE ftHandle;
FILE* frbf;

#define EXIT(status) \
{ FT_SetBitMode(ftHandle, 0x0, FT_BITMODE_RESET); \
  FT_Close(ftHandle); \
  fclose(frbf);\
  return status; }

static FT_STATUS
MPSSE_get_lbyte(BYTE *lbyte)
{
  DWORD dwNumBytesToSend, dwNumBytesSent, dwNumBytesToRead, dwNumBytesRead;
  BYTE byOutputBuffer[8];
  FT_STATUS ftStatus;

  dwNumBytesToSend = 0;
  byOutputBuffer[dwNumBytesToSend++] = MPSSE_CMD_GET_DATA_BITS_LOWBYTE;
  ftStatus = FT_Write(ftHandle, byOutputBuffer, dwNumBytesToSend, &dwNumBytesSent);
  Sleep(2); // Wait for data to be transmitted and status

  ftStatus = FT_GetQueueStatus(ftHandle, &dwNumBytesToRead);
  ftStatus |= FT_Read(ftHandle, lbyte, dwNumBytesToRead, &dwNumBytesRead);
  if ((ftStatus != FT_OK) & (dwNumBytesToRead != 1))
  {
    printf("Error read Lbyte\r\n");
    return FT_OTHER_ERROR; // Exit with error
  }
  return FT_OK;
}

static FT_STATUS
MPSSE_set_lbyte(BYTE lb, BYTE mask)
{
  DWORD dwNumBytesToSend, dwNumBytesSent;
  BYTE byOutputBuffer[8], lbyte;
  FT_STATUS ftStatus;

  ftStatus = MPSSE_get_lbyte(&lbyte);
  if ( ftStatus != FT_OK)
    return ftStatus;

  // Set to zero the bits selected by the mask:
  lbyte &= ~mask;
  // Setting zero is not selected by the mask bits:
  lb &= mask;

  lbyte |= lb;

  dwNumBytesToSend = 0;
  // Set data bits low-byte of MPSSE port:
  byOutputBuffer[dwNumBytesToSend++] = MPSSE_CMD_SET_DATA_BITS_LOWBYTE;
  byOutputBuffer[dwNumBytesToSend++] = lbyte;
  byOutputBuffer[dwNumBytesToSend++] = PORT_DIRECTION;
  ftStatus = FT_Write(ftHandle, byOutputBuffer, dwNumBytesToSend, &dwNumBytesSent);

  if ((ftStatus != FT_OK) & (dwNumBytesSent != 1))
  {
    printf("Error set Lbyte\r\n");
    return FT_OTHER_ERROR;
  }
  return FT_OK;
}

static FT_STATUS
MPSSE_open (char *description)
{
  FT_STATUS ftStatus;

  ftStatus = FT_OpenEx(description, FT_OPEN_BY_DESCRIPTION, &ftHandle);
  if (ftStatus != FT_OK)
  {
    printf ("Open failure\r\n");
    return FT_DEVICE_NOT_OPENED;
  }
  printf ("Оpen \"%s\" OK\r\n",description);

  ftStatus |= FT_ResetDevice(ftHandle);
  //Purge USB receive buffer first by reading out all old data from FT2232H receive buffer:
  ftStatus |= FT_Purge(ftHandle, FT_PURGE_RX);
  //Set USB request transfer sizes to 64K:
  ftStatus |= FT_SetUSBParameters(ftHandle, 65536, 65536);
  //Disable event and error characters:
  ftStatus |= FT_SetChars(ftHandle, 0, 0, 0, 0);
  //Sets the read and write timeouts in milliseconds:
  ftStatus |= FT_SetTimeouts(ftHandle, 0, 5000);
  //Set the latency timer to 1mS (default is 16mS):
  ftStatus |= FT_SetLatencyTimer(ftHandle, 1);
  //Turn on flow control to synchronize IN requests:
  ftStatus |= FT_SetFlowControl(ftHandle, FT_FLOW_RTS_CTS, 0x00, 0x00);
  //Reset controller:
  ftStatus |= FT_SetBitMode(ftHandle, 0x0, FT_BITMODE_RESET);
  //Enable MPSSE mode:
  ftStatus |= FT_SetBitMode(ftHandle, 0x0, FT_BITMODE_MPSSE);

  if (ftStatus != FT_OK)
  {
    printf("Error in initializing the MPSSE %d\n", ftStatus);
    return FT_OTHER_ERROR;
  }

  Sleep(50); // Wait for all the USB stuff to complete and work
  return FT_OK;
}


static FT_STATUS
MPSSE_setup ()
{
  DWORD dwNumBytesToSend, dwNumBytesSent, dwNumBytesToRead, dwNumBytesRead;
  BYTE byOutputBuffer[8], byInputBuffer[8];
  FT_STATUS ftStatus;

  // Multple commands can be sent to the MPSSE with one FT_Write
  dwNumBytesToSend = 0; // Start with a fresh index
  byOutputBuffer[dwNumBytesToSend++] = MPSSE_CMD_DISABLE_DIVIDER_5;
  byOutputBuffer[dwNumBytesToSend++] = MPSSE_CMD_DISABLE_ADAPTIVE_CLK;
  byOutputBuffer[dwNumBytesToSend++] = MPSSE_CMD_DISABLE_3PHASE_CLOCKING;

  ftStatus = FT_Write(ftHandle, byOutputBuffer, dwNumBytesToSend, &dwNumBytesSent);

  dwNumBytesToSend = 0; // Reset output buffer pointer
  // Set TCK frequency
  // Command to set clock divisor:
  byOutputBuffer[dwNumBytesToSend++] = MPSSE_CMD_SET_TCK_DIVISION;
  // Set ValueL of clock divisor:
  byOutputBuffer[dwNumBytesToSend++] = MPSSE_DATA_SPEED_DIV_L(DATA_SPEED);
  // Set 0xValueH of clock divisor:
  byOutputBuffer[dwNumBytesToSend++] = MPSSE_DATA_SPEED_DIV_H(DATA_SPEED);

  ftStatus |= FT_Write(ftHandle, byOutputBuffer, dwNumBytesToSend, &dwNumBytesSent);

  dwNumBytesToSend = 0; // Reset output buffer pointer

  // Set initial states of the MPSSE interface
  // - low byte, both pin directions and output values
/*
| FPGA pin  | Pin Name | Pin | MPSSE  | Dir | def |
| --------- | -------- | --- | ------ |---- |-----|
| DCLK      | BDBUS0   | 38  | TCK/SK | Out |  0  |
| DATA[0]   | BDBUS1   | 39  | TDI/DO | Out |  1  |
| nCONFIG   | BDBUS2   | 40  | TDO/DI | Out |  1  |
| nSTATUS   | BDBUS3   | 41  | TMS/CS | In  |  1  |
| CONF_DONE | BDBUS4   | 43  | GPIOL0 | In  |  1  |
 */

  // Configure data bits low-byte of MPSSE port:
  byOutputBuffer[dwNumBytesToSend++] = MPSSE_CMD_SET_DATA_BITS_LOWBYTE;
  // Initial state config above:
  byOutputBuffer[dwNumBytesToSend++] = (DCLK_DEF << DCLK) | (DATA0_DEF << DATA0)
                                     | (N_CONFIG_DEF << N_CONFIG) | (N_STATUS_DEF << N_STATUS)
                                     | (CONF_DONE_DEF << CONF_DONE);
  // Direction config above:
  byOutputBuffer[dwNumBytesToSend++] = PORT_DIRECTION;

  ftStatus |= FT_Write(ftHandle, byOutputBuffer, dwNumBytesToSend, &dwNumBytesSent);
  // Send off the low GPIO config commands
  dwNumBytesToSend = 0; // Reset output buffer pointer

  // Set initial states of the MPSSE interface
  // - high byte, all input, Initial State -- 0.
  // Send off the high GPIO config commands:
  byOutputBuffer[dwNumBytesToSend++] = MPSSE_CMD_SET_DATA_BITS_HIGHBYTE;
  byOutputBuffer[dwNumBytesToSend++] = 0x00;
  byOutputBuffer[dwNumBytesToSend++] = 0x00;
  ftStatus |= FT_Write(ftHandle, byOutputBuffer, dwNumBytesToSend, &dwNumBytesSent);

  // Disable loopback:
  byOutputBuffer[dwNumBytesToSend++] = MPSSE_CMD_DISABLE_LOOP_TDI_TDO;
  ftStatus |= FT_Write(ftHandle, byOutputBuffer, dwNumBytesToSend, &dwNumBytesSent);

  Sleep(2); // Wait for data to be transmitted and status
  ftStatus = FT_GetQueueStatus(ftHandle, &dwNumBytesToRead);
  ftStatus |= FT_Read(ftHandle, byInputBuffer, dwNumBytesToRead, &dwNumBytesRead);

  if (ftStatus != FT_OK)
  {
    printf("Unknown error in initializing the MPSSE\r\n");
    return FT_OTHER_ERROR;
  }
  else if (dwNumBytesToRead > 0)
  {
    printf("Error in configuration the MPSSE, bad code:\r\n");

    for ( int i = 0; i < dwNumBytesToRead; i++)
      printf (" %02Xh", byInputBuffer[i]);

    printf("\r\n");
    return FT_INVALID_PARAMETER;
  }

  return FT_OK;
}

static BYTE byBuffer[MPSSE_PCK_SEND_SIZE + 3];
static FT_STATUS
MPSSE_send(BYTE * buff, DWORD dwBytesToWrite)
{
  DWORD dwNumBytesToSend = 0, dwNumBytesSent, bytes;
  FT_STATUS ftStatus;

  // Output on rising clock, no input
  // MSB first, clock a number of bytes out
  byBuffer[dwNumBytesToSend++] = MPSSE_CMD_LSB_DATA_OUT_BYTES_POS_EDGE;

  bytes = dwBytesToWrite -1;
  byBuffer[dwNumBytesToSend++] = (bytes) & 0xFF; // Length L
  byBuffer[dwNumBytesToSend++] = (bytes >> 8) & 0xFF; // Length H

  memcpy(&byBuffer[dwNumBytesToSend], buff, dwBytesToWrite);

  dwNumBytesToSend += dwBytesToWrite;
  ftStatus = FT_Write(ftHandle, byBuffer, dwNumBytesToSend, &dwNumBytesSent);

  if (ftStatus != FT_OK )
  {
    printf ("ERROR send data\r\n");
    return ftStatus;
  } else if (dwNumBytesSent != dwNumBytesToSend)
  {
    printf ("ERROR send data, send %d, sent %d\r\n", dwNumBytesSent, dwNumBytesToSend);
  }

  return FT_OK;
}

int main(int argc, char *argv[])
{
	FT_STATUS ftStatus;
  BYTE lowByte;

	DWORD numDevs; // create the device information list

  if ( argv[1] == NULL)
  {
    printf ("NO file\r\n");
    return -1;
  }

  frbf = fopen(argv[1],"rb");
  if (frbf == NULL)
  {
    printf ("Error open rbf\r\n");
    return -1;
  }

	ftStatus = FT_CreateDeviceInfoList(&numDevs);
	if ((numDevs == 0) || (ftStatus != FT_OK))
	{
	  printf("Error. FTDI devices not found in the system\r\n");
	  return -1;
	}
#if PRINT_DEVICE_LIST
  FT_DEVICE_LIST_INFO_NODE *devInfo;
	// allocate storage for list based on numDevs
  devInfo = (FT_DEVICE_LIST_INFO_NODE*)malloc(sizeof(FT_DEVICE_LIST_INFO_NODE)*numDevs);
  ftStatus = FT_GetDeviceInfoList(devInfo,&numDevs);

  if (ftStatus == FT_OK)
    for (int i = 0; i < numDevs; i++)
    {
      printf("Dev %d:\n",i);
      printf(" Flags = 0x%x\n",devInfo[i].Flags);
      printf(" Type = 0x%x\n",devInfo[i].Type);
      printf(" ID = 0x%x\n",devInfo[i].ID);
      printf(" LocId = 0x%x\n",devInfo[i].LocId);
      printf(" SerialNumber = %s\n",devInfo[i].SerialNumber);
      printf(" Description = %s\n",devInfo[i].Description);
    }
#endif //PRINT_DEVICE_LIST

  ftStatus = MPSSE_open ("LESO7 B");
  if (ftStatus != FT_OK)
  {
    printf("Error in MPSSE_open %d\n", ftStatus);
    EXIT(-1);
  }

  MPSSE_setup();
  if (ftStatus != FT_OK)
  {
    printf("Error in MPSSE_setup %d\n", ftStatus);
    EXIT(-1);
  }

  printf ("nConfig -> 0\r\n");
  MPSSE_set_lbyte(0, 1 << N_CONFIG);
//  Sleep(10);

  printf ("nConfig -> 1\r\n");
  MPSSE_set_lbyte(1 << N_CONFIG, 1 << N_CONFIG);

  if (MPSSE_get_lbyte(&lowByte) != FT_OK)
  {
    EXIT(-1);
  }

  if (((lowByte >> N_STATUS) & 1) == 0)
  {
    printf("Error. FPGA is not responding\r\n");
    EXIT(-1);
  }

  int i = 0;
  size_t  readBytes = 0;
  // Send the configuration file:
  do
  {
    readBytes = fread(buff, 1, MPSSE_PCK_SEND_SIZE, frbf);
    if (MPSSE_send(buff, readBytes) != FT_OK)
      EXIT(-1);

    putchar('*');
    if (!((++i)%16)) printf("\r\n");
  }
  while (readBytes == MPSSE_PCK_SEND_SIZE);

  printf("\r\n");

  memset(buff, 0x00, sizeof(buff));
  MPSSE_send(buff, 1);  // неужели ни кто не заметит эту странную строку?
  printf("Load complete\r\n");

  // wait CONF_DONE set
  // A low-to-high transition on the CONF_DONE pin indicates that the configuration is
  // complete and initialization of the device can begin.
  i = 0;
  do
  {
    if (MPSSE_get_lbyte(&lowByte) != FT_OK)
    {
      printf ("Error read CONF_DONE\r\n");
      EXIT(-1);
    }
    if (i++ > TIMEOUT_CONF_DONE)
    {
      printf ("Error CONF_DONE\r\n");
      EXIT(-1);
    }
    Sleep(2);
  }
  while (((lowByte >> CONF_DONE) & 1) == 0);
  printf("Configuration complete\r\n");

  FT_Close(ftHandle);
  fclose(frbf);
}
